#!/usr/bin/env python3
"""A tool for automatic transmission of interviews, conference calls, ..."""

from argparse import ArgumentError, ArgumentParser, FileType
from dataclasses import dataclass
from datetime import timedelta
from itertools import chain
from pathlib import Path

import deepspeech
from pydub import AudioSegment
from pydub.silence import detect_nonsilent

__version__ = '0.0.1'

FRAME_RATE = 16000


@dataclass(frozen=True)
class AudioChunk:
    name: str
    start: timedelta
    audio: AudioSegment

    @property
    def duration(self) -> timedelta:
        return timedelta(seconds=self.audio.duration_seconds)

    @property
    def end(self) -> timedelta:
        return self.start + self.duration

    def __repr__(self) -> str:
        return f"{self.name} @ {str(self.start)}"

    @property
    def framerate(self) -> int:
        return self.audio.frame_count(1000)

    def text(self, ds: deepspeech.Model) -> str:
        assert self.framerate == FRAME_RATE
        return ds.stt(self.audio.get_array_of_samples(), FRAME_RATE)


def chunk_track(track: AudioSegment, name: str, **kwargs):
    for start, end in detect_nonsilent(track, **kwargs):
        yield AudioChunk(name, timedelta(milliseconds=start), track[start:end])


def chunk_interview(track1: AudioSegment, name1: str,
                    track2: AudioSegment, name2: str, **kwargs):
    return sorted(chain(chunk_track(track1, name1, **kwargs),
                        chunk_track(track2, name2, **kwargs)),
                  key=lambda c: c.start)


def parse_args(args=None):
    parser = ArgumentParser(description='Transcribe an interview.')
    bin_file, txt_file = FileType('br'), FileType('r')

    def seconds(arg) -> timedelta:
        return timedelta(seconds=float(arg))

    # TransRIP arguments
    parser.add_argument('-l', '--left', type=str, default='Left',
                        help='Name of the speaker on the left.')
    parser.add_argument('-r', '--right', type=str, default='Right',
                        help='Name of the speaker on the right.')
    parser.add_argument('-p', '--print-durations', action='store_true',
                        help='Print the end time and duration of each fragment.')
    parser.add_argument('tracks', type=bin_file, nargs='+',
                        help='Left and right channels, combined or in 2 files.')

    # pydub.silence arguments
    parser.add_argument('-s', '--min-silence', type=seconds, default=seconds(3),
                        help='Minimum length of a silence for detection (in seconds).')
    parser.add_argument('-m', '--min-length', type=seconds, default=seconds(0.5),
                        help='Minimum length of a fragment for detection (in seconds).')
    parser.add_argument('-t', '--threshold', type=float, default=-30,
                        help='Noise level which is considered silent (in dBFS).')

    # Mozilla DeepSpeech arguments
    models_dir = Path(__file__).parent / 'models'
    parser.add_argument('--model', default=models_dir / 'output_graph.pbmm',
                        type=bin_file, help='Path to the Mozilla DeepSpeech model.')
    parser.add_argument('--alphabet', default=models_dir / 'alphabet.txt', type=txt_file,
                        help='Path to the alphabet used by the DeepSpeech network')
    parser.add_argument('--lm', default=models_dir / 'lm.binary', type=bin_file,
                        help='Path to the DeepSpeech language model')
    parser.add_argument('--trie', default=models_dir / 'trie', type=bin_file,
                        help='Path to the DeepSpeech model trie file')

    return parser.parse_args(args)


def main(args=None):
    from deepspeech.client import BEAM_WIDTH, LM_ALPHA, LM_BETA, N_CONTEXT, N_FEATURES
    import sys
    args = parse_args(args)

    if len(args.tracks) == 2:
        left, right = map(AudioSegment.from_wav, args.tracks)

    elif len(args.tracks) == 1:
        audio = AudioSegment.from_wav(args.tracks[0])
        assert audio.channels == 2
        left, right = audio.split_to_mono()

    else:
        raise ArgumentError('tracks',
                            f"Expected 1 or 2 file(s), got {len(args.tracks)}.")

    ds = deepspeech.Model(str(args.model), N_FEATURES, N_CONTEXT,
                          str(args.alphabet), BEAM_WIDTH)
    ds.enableDecoderWithLM(str(args.alphabet), str(args.lm), str(args.trie),
                           LM_ALPHA, LM_BETA)

    fs = left.frame_count(1000)
    assert fs == right.frame_count(1000)

    if fs != FRAME_RATE:
        print(f"Warning: sample rate ({fs/1000} kHz) isn't {FRAME_RATE/1000} kHz. "
              "Resampling might produce erratic speech recognition.",
              file=sys.stderr)

        left = left.set_frame_rate(FRAME_RATE)
        right = right.set_frame_rate(FRAME_RATE)

    print()
    for chunk in chunk_interview(left, args.left, right, args.right,
                                 silence_thresh=args.threshold,
                                 min_silence_len=args.min_silence.seconds * 1000):
        if chunk.duration < args.min_length:
            print(f"[DEBUG]: Skipping chunk {chunk.start} +{chunk.duration}")
            continue

        print(f"[{chunk.start}] {chunk.name}: {chunk.text(ds)}")
        if args.print_durations:
            print(f"[{chunk.end}]: +{chunk.duration}")
            print()


if __name__ == "__main__":
    main()
