[ -n "$VIRTUAL_ENV" ] || . ./.envrc

run() {
    echo '$' "$@"
    "$@"
    echo
}
