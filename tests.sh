#!/bin/sh -e
. ./.common.sh

run pip install -q -r dev-requirements.txt
run flake8 transrip.py
