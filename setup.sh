#!/bin/sh -e
. ./.common.sh
run pip3 install -U wheel flit
run flit install

VERSION="0.4.1"
FILENAME="deepspeech-${VERSION}-models.tar.gz"
SHA512="804e97c22f83beefca9e54056f1c6d55499440171fd6cf7737b83895a847ede5bbd3d65dd196280d15ac239259573757069d209401478cd2f6eddbbeff4aaf35"

if [ ! -d models/ ]; then
    trap 'run rm "${FILENAME}"' EXIT
    [ -e "${FILENAME}" ] || \
        run wget "https://github.com/mozilla/DeepSpeech/releases/download/v${VERSION}/${FILENAME}"
    echo "${SHA512}  ${FILENAME}" | run sha512sum -c
    run tar xvfz "${FILENAME}"
fi
